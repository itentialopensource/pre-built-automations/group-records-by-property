<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Group Records By Property

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)
*  [Examples](#examples)
*  [Additional Information](#additional-information)

## Overview

This JST allows IAP users to pass in an array of objects and group the objects based on a common property provided by the user. The result will be an object with keys of different values for the common property in the data and the corresponding values being arrays that contain the corresponding matching records.

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform : `^2022.1`

## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.


## How to Run

Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the section in your workflow where you would like to group an array of objects by a common property and add a  `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `groupRecordsByProperty` (the name of the internal JST).

3. After selecting the task, the transformation dialog will display.

4. The inputs to the JST would be the array of objects and the common property based on which records have been grouped.

5. Save your input and the task is ready to run inside of IAP.

## Attributes  

Attributes for the pre-built are outlined in the following tables.

**Input**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>array</code></td>
<td>The input data array.</td>
<td><code>array[objects]</code></td>
</tr>
<tr>
<td><code>groupByProperty</code></td>
<td>The common property in the objects based on the grouping of records.</td>
<td><code>string</code></td>
</tr>
</tbody>
</table>


**Output**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>output</code></td>
<td>The grouped data object with keys of different values for the common property in the data, and the values being arrays of corresponding matching records.</td>
<td><code>object</code></td>
</tr>
</tbody>
</table>

## Examples
The following examples describe how the pre-built will work for different inputs.

### Example 1 
***Input***
```
{
  "array": [
    {
      "fruit": "apple",
      "price": "42.2",
      "type": "fruit"
    },
    {
      "fruit": "banana",
      "price": "10.1",
      "type": "fruit"
    },
    {
      "fruit": "potato",
      "price": "20",
      "type": "vegetable"
    }
  ],
  "groupByProperty": "type"
}
  ```

***Output***
```
{
  "fruit": [
    {
      "fruit": "apple",
      "price": "42.2",
      "type": "fruit"
    },
    {
      "fruit": "banana",
      "price": "10.1",
      "type": "fruit"
    }
  ],
  "vegetable": [
    {
      "fruit": "potato",
      "price": "20",
      "type": "vegetable"
    }
  ]
}
  ```
    
### Example 2 
***Input*** 
```
{
  "array": [
    {
      "number": 43,
      "president": "George W. Bush",
      "took_office": "2001-01-20",
      "left_office": "2009-01-20",
      "party": "Republican"
    },
    {
      "number": 44,
      "president": "Barack Obama",
      "took_office": "2009-01-20",
      "left_office": "2017-01-20",
      "party": "Democratic"
    },
    {
      "number": 45,
      "president": "Donald J. Trump",
      "took_office": "2017-01-20",
      "left_office": null,
      "party": "Republican"
    }
  ],
  "groupByProperty": "party"
}
  ```

***Output***
```
{
  "Republican": [
    {
      "number": 43,
      "president": "George W. Bush",
      "took_office": "2001-01-20",
      "left_office": "2009-01-20",
      "party": "Republican"
    },
    {
      "number": 45,
      "president": "Donald J. Trump",
      "took_office": "2017-01-20",
      "left_office": null,
      "party": "Republican"
    }
  ],
  "Democratic": [
    {
      "number": 44,
      "president": "Barack Obama",
      "took_office": "2009-01-20",
      "left_office": "2017-01-20",
      "party": "Democratic"
    }
  ]
}
  ```
  
## Additional Information
Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
