
## 0.0.8 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/group-records-by-property!7

---

## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/group-records-by-property!6

---

## 0.0.6 [06-14-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/group-records-by-property!5

---

## 0.0.5 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/group-records-by-property!4

---

## 0.0.4 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/group-records-by-property!3

---

## 0.0.3 [05-13-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/group-records-by-property!2

---

## 0.0.2 [12-23-2020]

* [LB-438] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/group-records-by-property!1

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
